package com.coronado.bottombar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentoCalculadora#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentoCalculadora extends Fragment implements View.OnClickListener {

    //1. Crear los objetos que se "relacionarán" con los botones de la layout

    MaterialButton boton1, boton2, boton3,boton4, boton5,boton6,boton7,boton8,boton9,boton0;

    TextView expresion;

    public FragmentoCalculadora() {
        // Required empty public constructor
    }

    public static FragmentoCalculadora newInstance(String param1, String param2) {
        FragmentoCalculadora fragment = new FragmentoCalculadora();
        Bundle args = new Bundle();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_fragmento_calculadora, container, false);
        expresion=view.findViewById(R.id.expresion);

        //2. Relacionar el objeto boton1 con el elemento cuyo id sea boton_1
        boton1=view.findViewById(R.id.boton_1);
        boton2=view.findViewById(R.id.boton_2);
        boton3=view.findViewById(R.id.boton_3);
        boton4=view.findViewById(R.id.boton_4);
        boton5=view.findViewById(R.id.boton_5);
        boton6=view.findViewById(R.id.boton_6);
        boton7=view.findViewById(R.id.boton_7);
        boton8=view.findViewById(R.id.boton_8);
        boton9=view.findViewById(R.id.boton_9);
        boton0=view.findViewById(R.id.boton_0);

        //3. Convertir el boton1 en un listener


        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {

        //1. Crear un objeto que "represente" el botón al cual se le ha hecho click
        MaterialButton boton= (MaterialButton) view;

        String texto;

        texto= boton.getText().toString();

        //Tomar lo que está en esa expresión y agregarle el texto del botón

        expresion.setText(texto);

    }
}